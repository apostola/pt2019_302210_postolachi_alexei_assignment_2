package server;

import generator.*;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Casa implements Runnable{
	private BlockingQueue<Client> rand;
	private AtomicInteger timer = new AtomicInteger(0);
	private AtomicInteger clientiNr = new AtomicInteger(0);
	private int timpTotal = 0;
	private int nrClienti = 0;
	private int timpAsteptare = 0;
	boolean running = true;
	
	public Casa(BlockingQueue<Client> rand) {
		this.rand = rand;
	}
	
	public void adaugaClient(Client client) {
		try {
			rand.put(client);
			timer.incrementAndGet();
			clientiNr.incrementAndGet();
			nrClienti++;
		} catch (InterruptedException e) {
			System.out.println("Casa.adaugaClien("+client.getNume()+") > Eroare la introducere client in coada!");
		}
	}
	
	public void run() {
		Client c = null;
		while(running == true) {
			try {
				c = deservesteClient();
				timpTotal += c.getTimpProcesare();
				timpAsteptare += timpTotal - c.getTimpSosire();
				Thread.sleep(c.getTimpProcesare()*1000 + 1000);
				clientiNr.decrementAndGet();
				System.out.println("Deservit clientul: "+c.getNume());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Client deservesteClient() {
		Client c = null;
		try {
			c = rand.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return c;
	}
	
	public AtomicInteger getNrClienti() {
		return timer;
	}
	
	public long getStatistics() {
		return (timpTotal * 1000) / nrClienti;
	}
	
	public long getTimpAsteptare() {
		return (timpAsteptare * 1000) / nrClienti;
	}
	
	public BlockingQueue<Client> getRand() {
		return rand;
	}
	
	public void kill() {
		running = false;
		Thread.currentThread().interrupt();
	}
}
