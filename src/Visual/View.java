package Visual;

import java.awt.List;
import java.awt.Panel;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import generator.*;
import server.Casa;

public class View extends JFrame{
	private JPanel window;
	private JPanel consolePanel;
	private JPanel inputPanel;
	private JPanel activeOutput;
	private JPanel upperPanel;
	private ArrayList<JScrollPane> list;
	
	private JTextArea consoleText;
	private JButton startSimulation;
	
	private JTextField minArr;
	private JTextField maxArr;
	private JTextField minService;
	private JTextField maxService;
	private JTextField nrQueues;
	private JTextField simInterval;
	private JTextField clientiTotal;
	
	public View() {
		window = new JPanel();
		window.setLayout(new BoxLayout(window, BoxLayout.Y_AXIS));
		consolePanel = new JPanel();
		activeOutput = new JPanel();
		inputPanel = new JPanel();
		inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.Y_AXIS));
		list = new ArrayList<JScrollPane>();
		
		consoleText = new JTextArea(20, 50);
		consoleText.setEditable(false);
		startSimulation = new JButton("Simulate");
		
		minArr = new JTextField(5);
		maxArr = new JTextField(5);
		minService = new JTextField(5);
		maxService = new JTextField(5);
		nrQueues = new JTextField(5);
		simInterval = new JTextField(5);
		clientiTotal = new JTextField(5);
		
		upperPanel = new JPanel();
		upperPanel.add(inputPanel);
		upperPanel.add(consolePanel);
		
		consolePanel.setBorder(new TitledBorder(new EtchedBorder(), "Console log:"));
		JScrollPane scrollField = new JScrollPane(consoleText);
		scrollField.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		consolePanel.add(scrollField);
		
		JPanel minp = new JPanel();
		JPanel maxP = new JPanel();
		JPanel minSer = new JPanel();
		JPanel maxSer = new JPanel();
		JPanel nrQ = new JPanel();
		JPanel simInt = new JPanel();
		JPanel maxClients = new JPanel();
		
		minp.add(new JLabel("Timp de sosire minim:	"));
		minp.add(minArr);
		
		maxP.add(new JLabel("Timp de sosire maxim:	"));
		maxP.add(maxArr);
		
		minSer.add(new JLabel("Timp deservire Minim:	"));
		minSer.add(minService);
		
		maxSer.add(new JLabel("Timp de deservire Maxim:		"));
		maxSer.add(maxService);
		
		nrQ.add(new JLabel("Numar cozi:	"));
		nrQ.add(nrQueues);
		
		simInt.add(new JLabel("Intervalul de simulare:	"));
		simInt.add(simInterval);
		
		maxClients.add(new JLabel("Nr de clienti: 	"));
		maxClients.add(clientiTotal);
		
		inputPanel.add(minp);
		inputPanel.add(maxP);
		inputPanel.add(minSer);
		inputPanel.add(maxSer);
		inputPanel.add(nrQ);
		inputPanel.add(simInt);
		inputPanel.add(maxClients);
		inputPanel.add(startSimulation);
		
		window.add(upperPanel);
		window.add(activeOutput);
		
		this.setContentPane(window);
		this.setTitle("Concurent-Queueing-App");
		this.setSize(1024, 1024);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public void clearActiveOutput() {
		activeOutput.removeAll();
		list.clear();
	}
	
	public void addElements(Casa[] clienti, int nrCase) {
		JPanel[] panels = new JPanel[nrCase];
		int i = 0;
		for (Casa k: clienti) {
			panels[i] = new JPanel();
			panels[i].setSize(100, 200);
			panels[i].setBorder(new TitledBorder(new EtchedBorder(), "Casa "+ i));
			
			JTextArea klientii = new JTextArea();
			klientii.setSize(100, 200);

			JScrollPane scrollField = new JScrollPane(klientii);
			scrollField.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			
			panels[i].add(scrollField);
			
			for (Client klient: k.getRand()) {
				klientii.append(klient.getNume() + "\n");
			}
			
			activeOutput.add(panels[i]);
			i++;
		}	
		setVisible(true);
	}
		
	
	public void setActiveOutput() {
		for (JScrollPane pane : list) {
			activeOutput.add(pane);
		}
		
		this.setVisible(true);
	}
	
	public void addToConsole(String text) {consoleText.append(text + "\n");}
	public JTextArea getConsoleInfo() {return this.consoleText;}
	
	public void addStartListener(ActionListener startListener) {this.startSimulation.addActionListener(startListener);}
	
	public void setQueue(JPanel panel) {activeOutput = panel;}
	public int getMinStartTime() {return Integer.parseInt(minArr.getText());}
	public int getMaxStartTime() {return Integer.parseInt(maxArr.getText());}
	public int getMinServiceTime() {return Integer.parseInt(minService.getText());}
	public int getMaxServiceTime() {return Integer.parseInt(maxService.getText());}
	public int getNrOfQueues() {return Integer.parseInt(nrQueues.getText());}
	public int getSimulationTime() {return Integer.parseInt(simInterval.getText());}
	public int getNrCLienti() {return Integer.parseInt(clientiTotal.getText());}
	
}
