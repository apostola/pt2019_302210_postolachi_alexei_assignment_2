package test;
import Visual.View;
import generator.Simulator;

public class Execute {
	public static void main(String args[]) {
		View view = new View();
		Simulator sim = new Simulator(view);
		Thread thread = new Thread(sim);
		thread.start();
	}
}
