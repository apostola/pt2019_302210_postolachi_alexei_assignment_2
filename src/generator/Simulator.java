package generator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;

import javax.swing.JOptionPane;
import org.omg.PortableInterceptor.ClientRequestInterceptor;

import Visual.View;
import server.Casa;

public class Simulator implements Runnable{
	
	View view = null;
	private int minTimpSosire = 0;
	private int maxTimpSosire = 0;
	private int maxTimpProcesare = 0;
	private int minTimpProcesare = 0;
	private int nrDeCase = 0;
	private int timpMax = 0;
	private int nrClientiPerTotal = 20; 
	private static String[] names = {
			"Aaron", "Abangale", "Abel",
			"Bailey", "Boris", "Bono",
			"Cristian", "Carol", "Carter",
			"Dalia", "Dana", "Diana",
			"Eliot", "Emerson", "Elizabeta",
			"Florin", "Flora", "Maxim"
			
	};
	private int currTime;
	private int runn = 0;
	
	Random random = new Random();
	
	private Menager menager;
	private ArrayList<Client> clienti;
	
	public Simulator(View view) {
		this.view = view;
		view.addStartListener(new startListener());
		menager = new Menager(nrDeCase, 10);
		clienti = new ArrayList<Client>();
		generateNRandomClienti();
		currTime = 0;
	}
	
	@Override
	public void run() {
		while (runn == 0) {
			try {
				System.out.println(0);
				Thread.sleep(1000);
			} catch (Exception e) {}
		}
		while (true) {
			Iterator<Client> iter = clienti.iterator();
			Client c = iter.next();
			while(currTime < timpMax) {
				System.out.println(currTime);
				view.addToConsole("\n"+currTime);
				while(iter.hasNext() && c.getTimpSosire() == currTime) {
					menager.addTask(c);
					view.addToConsole("Adaugat clientul: "+c.getNume() 
							+" Timp de sosire: "+c.getTimpSosire()+" Timp de asteptare: "+
							c.getTimpProcesare());
					c = iter.next();
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				updateUI();
				currTime++;
			}
			view.addToConsole(menager.getStatistics());
			view.addToConsole(menager.getPikeClients());
			view.addToConsole(menager.getTimpAsteptare());
			
			menager.endThreads();
			break;
		}
	}	
	
	private void generateNRandomClienti() {
		for (int i = 0; i < nrClientiPerTotal; i++) {
			int randomArr = random.nextInt(maxTimpSosire - minTimpSosire + 1) + minTimpSosire;
			int randomWait = random.nextInt(maxTimpProcesare - minTimpProcesare + 1) + minTimpProcesare;
			
			Client client = new Client(randomArr, randomWait, names[random.nextInt(18)]);
			clienti.add(client);
		}
		
		Collections.sort(clienti, new Comparator<Client>() {
			public int compare(Client o1, Client o2) {
				return o1.getTimpSosire() - o2.getTimpSosire();
			}
		});
	}
	
	public class startListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			try {
				minTimpSosire = view.getMinStartTime();
				maxTimpSosire = view.getMaxStartTime();				minTimpProcesare = view.getMinServiceTime();
				maxTimpProcesare = view.getMaxServiceTime();
				nrDeCase = view.getNrOfQueues();
				timpMax = view.getSimulationTime();
				nrClientiPerTotal = view.getNrCLienti();
				
				menager = new Menager(nrDeCase, 10);
				clienti.clear();
				generateNRandomClienti();
				runn = 1;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(view, "Eroare la citirea datelor");
			}
		}
	}
	
	private void updateUI() {
		view.clearActiveOutput();
		Casa[] casei = menager.listCase();
		view.addElements(casei, nrDeCase);
//		view.setActiveOutput();
	}

}
