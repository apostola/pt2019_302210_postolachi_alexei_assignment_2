package generator;

public class Client {
	private String nume = "";
	private int timpSosire;
	private int timpProcesare;
	
	public Client(int timpSosire, int timpProcesare, String nume) {
		this.timpSosire = timpSosire;
		this.timpProcesare = timpProcesare;
		this.nume = nume;
	}
	
	public String toString() {
		return "Clientul: "+nume+" Timp de sosire: "+timpSosire
				+" Timp de procesare: "+timpProcesare;
	}
	
	public String getNume() {return nume;}
	public int getTimpProcesare() {return timpProcesare;}
	public int getTimpSosire() {return timpSosire;}
}
