package generator;

import java.awt.Font;
import java.util.concurrent.ArrayBlockingQueue;

import server.Casa;

public class Menager {
	private Casa[] blocCase;
	private int nrCase;
	private int maxNrClientiPerCasa;
	
	public Menager(int nrCase, int maxNrClientiPerCasa) {
		this.nrCase = nrCase;
		this.maxNrClientiPerCasa = maxNrClientiPerCasa;
		blocCase = new Casa[nrCase];
		
		for(int i = 0; i < nrCase; i++) {
			blocCase[i] = new Casa(new ArrayBlockingQueue<Client>(maxNrClientiPerCasa));
			new Thread(blocCase[i]).start();
		}
	}
	
	public void endThreads() {
		for (int i =0; i < nrCase; i++) {
			blocCase[i].kill();		
		}
	}
	
	public String getStatistics() {
		long statistics = 0;
		for(int i = 0; i < nrCase; i++) {
			statistics += blocCase[i].getStatistics();
		}
		statistics = statistics / nrCase;
		return "Timp de procesare: "+statistics;
	}
	
	public String getTimpAsteptare() {
		long statistics = 0;
		for(int i = 0; i < nrCase; i++) {
			statistics += blocCase[i].getTimpAsteptare();
		}
		statistics = statistics / nrCase;
		return "Timp asteptare: "+statistics;
	}
	
	public String getPikeClients() {
		int statistics = 0;
		int pos = 0;
		for(int i = 0; i < nrCase; i++) {
			int clienti = 0;
			clienti = blocCase[i].getNrClienti().get();
			if (clienti > statistics) {
				statistics = clienti;
				pos = i + 1;
			}
		}
		
		return "Pike: Casa "+pos+"Nr clienti: "+statistics;
	}
	
	public Casa[] listCase() {
		return blocCase;
	}
	
	public void addTask(Client c) {
		int nrCasa = 0;	// Casa care se va alege;
		int min = blocCase[0].getNrClienti().intValue(); // nrClienti la prima casa
		for (int i = 1; i < nrCase; i++) {
			int local = blocCase[i].getNrClienti().intValue();
			if (local < min) { // daca casa curenta are mai putini clienti decat casa nu nr nrCasa
				min = local; // min <= nrDeClienti din casa curenta
				nrCasa = i; // nr casei cu cei mai putini clienti
			}
		}
		System.out.println("Adaugat la casa: "+nrCasa);
		blocCase[nrCasa].adaugaClient(c);
	}
}
